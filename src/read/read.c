/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 14:45:48 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 17:44:49 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	validav(char *str)
{
	int n;

	n = 0;
	if ((ft_atoi(str) < MIN) || (ft_atoi(str) > MAX))
		puterror("Error");
	if (((str[0] == '-') || (str[0] == '+')) &&
		(str[1] >= '0' && str[1] <= '9'))
		n = 1;
	while (str[n])
	{
		if (str[n] >= '0' && str[n] <= '9')
		{
			n++;
			continue ;
		}
		puterror("Error");
	}
}

static void	checkarr(char **av)
{
	int i;
	int j;

	i = 0;
	while (av[i])
	{
		j = 0;
		while (av[j])
		{
			if (i == j)
			{
				j++;
				continue ;
			}
			if (!ft_strcmp(av[i], av[j]))
				puterror("Error");
			j++;
		}
		i++;
	}
}

static void	create_a(char **av, t_root **root)
{
	t_stack *stack;

	checkarr(av);
	*root = create_root();
	validav(*av);
	(*root)->a = create_stack(ft_atoi(*av++));
	stack = (*root)->a;
	while (*av)
	{
		validav(*av);
		stack->next = create_stack(ft_atoi(*av));
		stack = stack->next;
		av++;
	}
}

static void	cleararrint(char **arr)
{
	int i;

	i = 0;
	while (arr[i])
	{
		free(arr[i]);
		i++;
	}
	free(arr);
	arr = NULL;
}

void		ft_read(int ac, char **av, t_root **root)
{
	char	**arr;

	if (ac == 2)
	{
		arr = ft_strsplit(av[0], ' ');
		create_a(arr, root);
		cleararrint(arr);
	}
	else
		create_a(av, root);
}
