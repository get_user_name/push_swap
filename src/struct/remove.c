/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 21:42:46 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 19:36:24 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

void	remove_stack(t_stack **data)
{
	(void)(*data)->num;
	(*data)->next = NULL;
	free(*data);
	*data = NULL;
}

void	remove_firstack(t_stack **data)
{
	t_stack *stack;

	if ((*data)->next)
	{
		stack = (*data)->next;
		remove_stack(data);
		(*data) = stack;
	}
	else
	{
		if (*data)
			remove_stack(data);
	}
}

void	remove_move(t_move **data)
{
	(void)(*data)->a;
	(void)(*data)->b;
	(void)(*data)->ab;
	(*data)->next = NULL;
	free(*data);
	*data = NULL;
}

void	remove_valid(t_valid **data)
{
	(void)(*data)->move;
	(void)(*data)->right;
	(void)(*data)->left;
	(*data)->next = NULL;
	free(*data);
	*data = NULL;
}
