/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 19:55:58 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/16 19:08:02 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

t_root		*create_root(void)
{
	t_root	*new;

	if (!(new = (t_root*)malloc(sizeof(t_root))))
		exit(EXIT_FAILURE);
	new->move = 0;
	new->a = NULL;
	new->b = NULL;
	new->history = NULL;
	new->pic = NULL;
	new->flags = NULL;
	return (new);
}

t_stack		*create_stack(int num)
{
	t_stack	*new;

	if (!(new = (t_stack*)malloc(sizeof(t_stack))))
		exit(EXIT_FAILURE);
	new->num = num;
	new->next = NULL;
	return (new);
}

t_history	*create_history(char *str)
{
	t_history	*new;

	if (!(new = (t_history*)malloc(sizeof(t_history))))
		exit(EXIT_FAILURE);
	new->value = str;
	new->next = NULL;
	return (new);
}

t_valid		*create_valid(int move, int left, int right)
{
	t_valid	*new;

	if (!(new = (t_valid*)malloc(sizeof(t_valid))))
		exit(EXIT_FAILURE);
	new->move = move;
	new->right = right;
	new->left = left;
	new->next = NULL;
	return (new);
}

t_move		*create_move(int a, int b)
{
	t_move	*new;

	if (!(new = (t_move*)malloc(sizeof(t_move))))
		exit(EXIT_FAILURE);
	new->a = a;
	new->b = b;
	new->ab = 0;
	new->next = NULL;
	return (new);
}
