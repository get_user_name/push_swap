/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:36:20 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:36:25 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

int	end_num(t_stack *stack)
{
	while (stack->next)
		stack = stack->next;
	return (stack->num);
}

int	before_last(t_stack *stack)
{
	int	answer;

	while (stack->next)
	{
		answer = stack->num;
		stack = stack->next;
	}
	return (answer);
}
