/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_debug.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 18:32:03 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/16 18:39:30 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

t_picture	*create_picture(int width, int height)
{
	t_picture	*new;

	if (!(new = (t_picture*)malloc(sizeof(t_picture))))
		exit(EXIT_FAILURE);
	new->width = width;
	new->height = height;
	new->mlx_ptr = NULL;
	new->win_ptr = NULL;
	new->map = NULL;
	return (new);
}

t_map		*create_map(t_line *line)
{
	t_map	*new;

	if (!(new = (t_map*)malloc(sizeof(t_map))))
		exit(EXIT_FAILURE);
	new->line = line;
	new->next = NULL;
	return (new);
}

t_line		*create_line(int color)
{
	t_line	*new;

	if (!(new = (t_line*)malloc(sizeof(t_line))))
		exit(EXIT_FAILURE);
	new->color = color;
	new->next = NULL;
	return (new);
}
