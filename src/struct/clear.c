/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:32:58 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/16 17:29:15 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	clearstack(t_stack **stack)
{
	t_stack *next;

	while (*stack)
	{
		next = (*stack)->next;
		remove_stack(stack);
		*stack = next;
	}
}

static void	clearhis(t_history **his)
{
	t_history *next;

	while (*his)
	{
		next = (*his)->next;
		free((*his)->value);
		free(*his);
		*his = NULL;
		*his = next;
	}
}

static void	cleararrchar(char ***str)
{
	int i;

	i = 0;
	while (*str && (*str)[i])
	{
		free((*str)[i]);
		i++;
	}
	free(*str);
	*str = NULL;
}

void		clearroot(t_root **root)
{
	(void)(*root)->move;
	clearstack(&(*root)->a);
	clearstack(&(*root)->b);
	clearhis(&(*root)->history);
	cleararrchar(&(*root)->flags);
	free(*root);
	*root = NULL;
}

void		clear_move(t_move **data)
{
	t_move *next;

	while (*data)
	{
		next = (*data)->next;
		remove_move(data);
		*data = next;
	}
}
