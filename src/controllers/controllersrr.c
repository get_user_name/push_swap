/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controllersrr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:26:15 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:27:04 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	rrrra(t_root **root)
{
	t_stack *next;
	t_stack *value;

	next = (*root)->a;
	while (next->next->next)
		next = next->next;
	value = next->next;
	next->next = NULL;
	value->next = (*root)->a;
	(*root)->a = value;
}

static void	rrrrb(t_root **root)
{
	t_stack *next;
	t_stack *value;

	next = (*root)->b;
	while (next->next->next)
		next = next->next;
	value = next->next;
	next->next = NULL;
	value->next = (*root)->b;
	(*root)->b = value;
}

int			rra(t_root **root)
{
	if (!(*root)->a || !(*root)->a->next)
		return (1);
	rrrra(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("rra")));
	return (0);
}

int			rrb(t_root **root)
{
	if (!(*root)->b || !(*root)->b->next)
		return (1);
	rrrrb(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("rrb")));
	return (0);
}

int			rrr(t_root **root)
{
	if (!(*root)->b || !(*root)->b->next ||
		!(*root)->a || !(*root)->a->next)
		return (1);
	rrrra(root);
	rrrrb(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("rrr")));
	return (0);
}
