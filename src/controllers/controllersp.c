/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controllersp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:25:36 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:25:41 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

int		pa(t_root **root)
{
	t_stack *value;

	if (!(*root)->b)
		return (1);
	value = create_stack((*root)->b->num);
	value->next = (*root)->a;
	(*root)->a = value;
	remove_firstack(&(*root)->b);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("pa")));
	return (0);
}

int		pb(t_root **root)
{
	t_stack *value;

	if (!(*root)->a)
		return (1);
	value = create_stack((*root)->a->num);
	value->next = (*root)->b;
	(*root)->b = value;
	remove_firstack(&(*root)->a);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("pb")));
	return (0);
}
