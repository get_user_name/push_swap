/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controllers.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 18:02:08 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:17:43 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static int	controllerr(t_root **root, char *str, int i)
{
	if (i == 1 && !ft_strcmp(str, "rb"))
		rb(root);
	else if (i == 2 && !ft_strcmp(str, "rb"))
		rrb(root);
	else if (i == 1 && !ft_strcmp(str, "rr"))
		rr(root);
	else if (i == 2 && !ft_strcmp(str, "rr"))
		rrr(root);
	else if (i == 1 && !ft_strcmp(str, "rrr"))
		rrr(root);
	else if (i == 2 && !ft_strcmp(str, "rrr"))
		rr(root);
	else
		return (1);
	return (0);
}

static int	controllerrr(t_root **root, char *str, int i)
{
	if (i == 1 && !ft_strcmp(str, "rra"))
		rra(root);
	else if (i == 2 && !ft_strcmp(str, "rra"))
		ra(root);
	else if (i == 1 && !ft_strcmp(str, "ra"))
		ra(root);
	else if (i == 2 && !ft_strcmp(str, "ra"))
		rra(root);
	else if (i == 1 && !ft_strcmp(str, "rrb"))
		rrb(root);
	else if (i == 2 && !ft_strcmp(str, "rrb"))
		rb(root);
	else
		return (1);
	return (0);
}

static int	controllersp(t_root **root, char *str, int i)
{
	if ((i == 1 || i == 2) && !ft_strcmp(str, "sa"))
		sa(root);
	else if ((i == 1 || i == 2) && !ft_strcmp(str, "sb"))
		sb(root);
	else if ((i == 1 || i == 2) && !ft_strcmp(str, "pa"))
		pa(root);
	else if ((i == 1 || i == 2) && !ft_strcmp(str, "pb"))
		pb(root);
	else
		return (1);
	return (0);
}

void		controllers(t_root **root, char *str, int i)
{
	if (controllerr(root, str, i)
		&& controllerrr(root, str, i)
		&& controllersp(root, str, i))
		puterror("Error");
}
