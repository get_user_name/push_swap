/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controllersr.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:25:53 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:26:09 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

void	universalr(t_stack **stack)
{
	push_stack(stack, create_stack((*stack)->num));
	remove_firstack(stack);
}

int		ra(t_root **root)
{
	if (!(*root)->a || !(*root)->a->next)
		return (1);
	universalr(&(*root)->a);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("ra")));
	return (0);
}

int		rb(t_root **root)
{
	if (!(*root)->b || !(*root)->b->next)
		return (1);
	universalr(&(*root)->b);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("rb")));
	return (0);
}

int		rr(t_root **root)
{
	if (!(*root)->b || !(*root)->b->next ||
		!(*root)->a || !(*root)->a->next)
		return (1);
	universalr(&(*root)->a);
	universalr(&(*root)->b);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("rr")));
	return (0);
}
