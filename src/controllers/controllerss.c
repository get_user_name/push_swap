/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   controllerss.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/15 13:27:12 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:27:29 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	sssa(t_root **root)
{
	ft_swap(&(*root)->a->num, &(*root)->a->next->num);
}

static void	sssb(t_root **root)
{
	ft_swap(&(*root)->b->num, &(*root)->b->next->num);
}

int			sa(t_root **root)
{
	if (!(*root)->a || !(*root)->a->next)
		return (1);
	sssa(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("sa")));
	return (0);
}

int			sb(t_root **root)
{
	if (!(*root)->b || !(*root)->b->next)
		return (1);
	sssb(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("sb")));
	return (0);
}

int			ss(t_root **root)
{
	if (!(*root)->a || !(*root)->a->next ||
			!(*root)->b || !(*root)->b->next)
		return (1);
	sssa(root);
	sssb(root);
	(*root)->move += 1;
	push_history(&(*root)->history, create_history(ft_strdup("ss")));
	return (0);
}
