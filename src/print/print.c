/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 15:32:45 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:08:47 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	print(t_root **root)
{
	printbar(root);
	printstacka(root, (*root)->a);
	printstackb(root, (*root)->b);
}

static int	deal_key(int key, t_root *root)
{
	char	*line;

	line = NULL;
	if (key == 53)
	{
		checkerres(&root);
		clearroot(&root);
		ft_putendl("OK");
		exit(EXIT_FAILURE);
	}
	else if ((key == 124) && gnl(0, &line))
	{
		mlx_clear_window(root->pic->mlx_ptr, root->pic->win_ptr);
		controllers(&root, line, 1);
		putroot(root);
		free(line);
		print(&root);
	}
	return (0);
}

static void	start_win(t_picture **pic)
{
	(*pic) = create_picture(1024, 1024);
	(*pic)->mlx_ptr = mlx_init();
	(*pic)->win_ptr = mlx_new_window((*pic)->mlx_ptr,
		(*pic)->width, (*pic)->height, "Debugger V 0.1");
}

void		startdebug(t_root **root)
{
	t_picture *pic;

	start_win(&pic);
	(*root)->pic = pic;
	print(root);
	putroot(*root);
	mlx_hook(pic->win_ptr, 2, 0, deal_key, *root);
	mlx_loop(pic->mlx_ptr);
}
