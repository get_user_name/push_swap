/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bar.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 15:35:07 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:08:00 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	printline(int a, int b, int y, t_picture *pic)
{
	while (a < b)
	{
		mlx_pixel_put(pic->mlx_ptr, pic->win_ptr, a, y, 0xFF4500);
		a++;
	}
}

static void	printrow(int a, int b, int x, t_picture *pic)
{
	while (a < b)
	{
		mlx_pixel_put(pic->mlx_ptr, pic->win_ptr, x, a, 0xFFD700);
		a++;
	}
}

static void	putmat(t_root **root)
{
	int	n;
	int i;
	int count;

	i = 0;
	count = (*root)->pic->width / (countstack((*root)->a) +
		countstack((*root)->b)) * countstack((*root)->a);
	n = (*root)->pic->height / 13 * 2;
	while (i <= n)
	{
		printline(0, count, i, (*root)->pic);
		i++;
	}
	i = 0;
	n++;
	while (count <= (*root)->pic->width)
	{
		printrow(i, n, count, (*root)->pic);
		count++;
	}
}

void		printbar(t_root **root)
{
	putmat(root);
	mlx_string_put((*root)->pic->mlx_ptr, (*root)->pic->win_ptr,
		20, 40, 0xFFFFFF, "Stack A = ");
	mlx_string_put((*root)->pic->mlx_ptr, (*root)->pic->win_ptr,
		120, 40, 0xFFFFFF, ft_itoa(countstack((*root)->a)));
	mlx_string_put((*root)->pic->mlx_ptr, (*root)->pic->win_ptr,
		20, 80, 0xFFFFFF, "Stack B = ");
	mlx_string_put((*root)->pic->mlx_ptr, (*root)->pic->win_ptr,
		120, 80, 0xFFFFFF, ft_itoa(countstack((*root)->b)));
}
