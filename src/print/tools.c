/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 15:56:01 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:23:20 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

void	checkerres(t_root **root)
{
	t_stack *stack;

	stack = (*root)->a;
	if ((*root)->b || !(*root)->a)
		puterror("KO");
	while (stack->next)
	{
		if (stack->num > stack->next->num)
			puterror("KO");
		stack = stack->next;
	}
}

void	runworker(t_root **root)
{
	char	*line;

	line = NULL;
	while (gnl(0, &line))
	{
		controllers(root, line, 1);
		free(line);
	}
}

int		getflag(char **flag, char *str)
{
	int i;

	i = 0;
	while (flag[i])
	{
		if (!ft_strcmp(flag[i], str))
			return (1);
		i++;
	}
	return (0);
}

void	worker(t_root **root)
{
	if ((*root)->flags == NULL)
		runworker(root);
	else if (getflag((*root)->flags, "h"))
		startdebug(root);
	else
		puterror("KO");
	checkerres(root);
	clearroot(root);
	ft_putendl("OK");
}
