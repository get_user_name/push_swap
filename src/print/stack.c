/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 15:51:58 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:11:47 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	putlinesta(t_root **root, int y, int x, int n)
{
	while (x++ < n)
		mlx_pixel_put((*root)->pic->mlx_ptr,
			(*root)->pic->win_ptr, x - 1, y, 0x9ACD32);
	x--;
	while (x++ < (*root)->pic->width / 2)
		mlx_pixel_put((*root)->pic->mlx_ptr,
			(*root)->pic->win_ptr, x - 1, y, 0x6B8E23);
}

void		printstacka(t_root **root, t_stack *stack)
{
	int	y;
	int x;
	int	n;
	int	height;

	y = (*root)->pic->height / 13 * 2;
	while (stack)
	{
		height = y + ((*root)->pic->height - (*root)->pic->height / 13 * 2)
			/ (countstack((*root)->a));
		while (y <= height)
		{
			x = 0;
			n = (*root)->pic->width / 2 - (*root)->pic->width / 2 /
			(countstack((*root)->a) + countstack((*root)->b)) * abs(stack->num);
			putlinesta(root, y, x, n);
			y++;
		}
		stack = stack->next;
	}
}

static void	putlinestb(t_root **root, int y)
{
	int x;

	while (y < (*root)->pic->height)
	{
		x = (*root)->pic->width / 2;
		while (x < (*root)->pic->width)
		{
			mlx_pixel_put((*root)->pic->mlx_ptr,
				(*root)->pic->win_ptr, x - 1, y, 0xFF7F50);
			x++;
		}
		y++;
	}
}

void		printstackb(t_root **root, t_stack *stack)
{
	int	y;
	int x;
	int	height;

	y = (*root)->pic->height / 13 * 2;
	while (stack)
	{
		height = y + ((*root)->pic->height - (*root)->pic->height / 13 * 2)
			/ (countstack((*root)->b));
		while (y <= height)
		{
			x = (*root)->pic->width / 2;
			while (x++ < (*root)->pic->width / 2 + (*root)->pic->width / 2 /
			(countstack((*root)->a) + countstack((*root)->b)) * abs(stack->num))
				mlx_pixel_put((*root)->pic->mlx_ptr,
					(*root)->pic->win_ptr, x - 1, y, 0xFF6347);
			x--;
			while (x++ < (*root)->pic->width)
				mlx_pixel_put((*root)->pic->mlx_ptr,
					(*root)->pic->win_ptr, x - 1, y, 0xFF7F50);
			y++;
		}
		stack = stack->next;
	}
	putlinestb(root, y);
}
