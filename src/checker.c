/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 19:09:53 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:19:46 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	checker(int ac, char **av)
{
	t_root	*root;
	char	**flags;

	flags = NULL;
	if (checkflag(&ac, &av, "h"))
		push_ch_arr(&flags, "h");
	ft_read(ac, av, &root);
	root->flags = flags;
	worker(&root);
}

int		main(int ac, char **av)
{
	if (ac > 1)
		checker(ac, &av[1]);
	else
		ft_putendl("");
	return (0);
}
