/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 20:49:53 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 16:15:48 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	putstack(t_stack *stack)
{
	while (stack)
	{
		ft_putnbr(stack->num);
		ft_putstr(" ");
		stack = stack->next;
	}
	ft_putendl("");
}

void		puthistory(t_history *history)
{
	while (history)
	{
		ft_putstr(history->value);
		ft_putendl("");
		history = history->next;
	}
}

void		puterror(char *str)
{
	ft_putendl(str);
	exit(EXIT_FAILURE);
}

void		putroot(t_root *root)
{
	ft_putendl("");
	ft_putstr("A : ");
	putstack(root->a);
	ft_putstr("B : ");
	putstack(root->b);
}
