/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 19:09:53 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 17:44:38 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	push_swap(int ac, char **av)
{
	t_root	*root;

	ft_read(ac, av, &root);
	sort(&root);
}

int		main(int ac, char **av)
{
	if (ac > 1)
		push_swap(ac, &av[1]);
	else
		ft_putendl("");
	return (0);
}
