/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   choice.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 21:02:03 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 14:09:40 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	optimiz(t_move **move)
{
	int	min;

	if ((*move)->a == (*move)->b)
	{
		(*move)->ab = (*move)->a;
		(*move)->a = 0;
		(*move)->b = 0;
	}
	else if (((*move)->a < 0) && ((*move)->b < 0))
	{
		min = revabs(getmin(abs((*move)->a), abs((*move)->b)));
		(*move)->ab = min;
		(*move)->a -= min;
		(*move)->b -= min;
	}
	else if (((*move)->a > 0) && ((*move)->b > 0))
	{
		min = getmin((*move)->a, (*move)->b);
		(*move)->ab = min;
		(*move)->a -= min;
		(*move)->b -= min;
	}
}

static void	choosemove(t_move **move, int a, int b)
{
	t_move *spare;

	if (!*move)
		*move = create_move(a, b);
	spare = create_move(a, b);
	optimiz(&spare);
	if ((abs(spare->a) + abs(spare->b) + abs(spare->ab)) <
		(abs((*move)->a) + abs((*move)->b) + abs((*move)->ab)))
	{
		(*move)->a = spare->a;
		(*move)->b = spare->b;
		(*move)->ab = spare->ab;
	}
	remove_move(&spare);
}

static void	set_move(t_root **root, t_move **move)
{
	repeatrr((*move)->ab, root);
	repeata((*move)->a, root);
	repeatb((*move)->b, root);
	remove_move(move);
	pa(root);
}

void		choice(t_root **root)
{
	int			count;
	int			i;
	t_stack		*stack;
	t_move		*move;

	stack = (*root)->b;
	move = NULL;
	count = countstack(stack);
	i = count;
	while (stack)
	{
		if (i > (count / 2))
			choosemove(&move, insert((*root)->a, (*root)->a,
				stack->num), count - i);
		else
			choosemove(&move, insert((*root)->a, (*root)->a, stack->num), -i);
		stack = stack->next;
		i--;
	}
	set_move(root, &move);
}
