/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   root.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 21:04:27 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 18:25:41 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

void		sort(t_root **root)
{
	int count;

	count = countstack((*root)->a);
	if (checksorted((*root)->a))
	{
		if (count == 2)
		{
			if ((*root)->a->num > (*root)->a->next->num)
				sa(root);
		}
		else if (count == 3)
			unzip3(root);
		else if (count > 3)
		{
			repeatpb(count - 3, root);
			unzip3(root);
			while ((*root)->b)
				choice(root);
		}
	}
	repeata(setplay((*root)->a, countstack((*root)->a)), root);
	puthistory((*root)->history);
	clearroot(root);
}
