/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   repeat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 20:46:30 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 13:30:51 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

void	repeatrr(int a, t_root **root)
{
	if (a > 0)
		while (a)
		{
			rr(root);
			a--;
		}
	else if (a < 0)
		while (a)
		{
			rrr(root);
			a++;
		}
}

void	repeata(int a, t_root **root)
{
	if (a > 0)
		while (a)
		{
			ra(root);
			a--;
		}
	else if (a < 0)
		while (a)
		{
			rra(root);
			a++;
		}
}

void	repeatb(int a, t_root **root)
{
	if (a > 0)
		while (a)
		{
			rb(root);
			a--;
		}
	else if (a < 0)
		while (a)
		{
			rrb(root);
			a++;
		}
}

void	repeatpb(int a, t_root **root)
{
	while (a)
	{
		pb(root);
		a--;
	}
}
