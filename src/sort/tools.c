/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 20:27:32 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 14:58:32 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

int		countstack(t_stack *stack)
{
	int n;

	n = 0;
	while (stack && ++n)
		stack = stack->next;
	return (n);
}

int		getmin(int a, int b)
{
	if (a <= b)
		return (a);
	return (b);
}

int		getmax(int a, int b)
{
	if (a <= b)
		return (b);
	return (a);
}

void	unzip3(t_root **root)
{
	t_stack *stack;

	stack = (*root)->a;
	if (((stack->num > stack->next->num) &&
		(stack->next->num > stack->next->next->num) &&
		(stack->num > stack->next->next->num)) ||
		((stack->num < stack->next->num) &&
		(stack->next->num > stack->next->next->num) &&
		(stack->num < stack->next->next->num)) ||
		((stack->num > stack->next->num) &&
		(stack->next->num < stack->next->next->num) &&
		(stack->num < stack->next->next->num)))
		sa(root);
}

int		setplay(t_stack *stack, int count)
{
	int move;
	int min;
	int i;

	min = stack->num;
	move = 0;
	i = count;
	while (stack)
	{
		if (min > stack->num)
		{
			min = stack->num;
			if (i > (count / 2))
				move = count - i;
			else
				move = -i;
		}
		i--;
		stack = stack->next;
	}
	return (move);
}
