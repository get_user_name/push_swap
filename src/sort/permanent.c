/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permanent.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 18:25:43 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 18:28:26 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static t_stack	*copystack(t_stack *stack)
{
	t_stack *copy;
	t_stack *next;

	copy = create_stack(stack->num);
	next = copy;
	while (stack->next)
	{
		next->next = create_stack(stack->next->num);
		next = next->next;
		stack = stack->next;
	}
	return (copy);
}

static void		clearstack(t_stack **stack)
{
	t_stack *next;

	while (*stack)
	{
		next = (*stack)->next;
		remove_stack(stack);
		*stack = next;
	}
}

static void		repeaun(int a, t_stack **stack)
{
	t_stack *next;
	t_stack *value;

	if (a > 0)
		while (a)
		{
			universalr(stack);
			a--;
		}
	else if (a < 0)
		while (a)
		{
			next = *stack;
			while (next->next->next)
				next = next->next;
			value = next->next;
			next->next = NULL;
			value->next = *stack;
			*stack = value;
			a++;
		}
}

int				checksorted(t_stack *stack)
{
	t_stack	*st;

	st = copystack(stack);
	repeaun(setplay(stack, countstack(stack)), &st);
	while (st->next)
	{
		if (st->num > st->next->num)
		{
			clearstack(&st);
			return (1);
		}
		st = st->next;
	}
	clearstack(&st);
	return (0);
}
