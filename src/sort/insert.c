/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/13 21:04:11 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/15 14:01:40 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/push_swap.h"

static void	choose(t_valid **valid, int move, int left, int right)
{
	if (!*valid)
		*valid = create_valid(move, left, right);
	if (abs(right - left) <
		abs((*valid)->right - (*valid)->left))
	{
		(*valid)->right = right;
		(*valid)->left = left;
		(*valid)->move = move;
	}
}

static int	decideins(t_valid **valid, t_stack *spare, int count)
{
	int answer;

	if (!*valid)
		return (setplay(spare, count));
	answer = (*valid)->move;
	remove_valid(valid);
	return (answer);
}

int			insert(t_stack *stack, t_stack *spare, int n)
{
	int		count;
	int		i;
	t_valid	*valid;

	valid = NULL;
	count = countstack(stack);
	i = count;
	while (stack->next)
	{
		i--;
		if ((stack->num <= n && stack->next->num >= n) ||
			(stack->num >= n && stack->next->num <= n))
		{
			if (i > (count / 2))
				choose(&valid, count - i, stack->num, stack->next->num);
			else
				choose(&valid, -i, stack->num, stack->next->num);
		}
		stack = stack->next;
	}
	if ((stack->num <= n && spare->num >= n) ||
		(stack->num >= n && spare->num <= n))
		choose(&valid, 0, stack->num, spare->num);
	return (decideins(&valid, spare, count));
}
