/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_ch_arr.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 18:13:10 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/16 18:13:22 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	push_ch_arr(char ***arr, char *str)
{
	char	**newarr;
	int		i;

	i = 0;
	while (*arr && (*arr)[i])
		i++;
	newarr = (char**)malloc(sizeof(char*) * (i + (!*arr ? 2 : 1)));
	i = 0;
	while (*arr && (*arr)[i])
	{
		newarr[i] = ft_strdup((*arr)[i]);
		i++;
	}
	newarr[i] = ft_strdup(str);
	newarr[i + 1] = NULL;
	clear_arr_char(&(*arr));
	*arr = newarr;
}
