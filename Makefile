# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/02/05 19:02:10 by gkoch             #+#    #+#              #
#    Updated: 2019/02/17 18:27:45 by gkoch            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #


NAME = $(NAME1) $(NAME2)

NAME1 = checker
NAME2 = push_swap

NAME_CH		= checker
PUT			= $(SRC)put/
STRUCT		= $(SRC)struct/
CONTROL		= $(SRC)controllers/
SORT		= $(SRC)sort/
READ		= $(SRC)read/
PRINT		= $(SRC)print/
SRC			= ./src/
INCLUDES	= includes/

SRCS_PATH = $(STRUCT)create.c $(STRUCT)push.c $(STRUCT)remove.c $(STRUCT)get.c \
										$(STRUCT)clear.c $(STRUCT)create_debug.c \
				\
				$(PUT)put.c \
				\
				$(CONTROL)controllersp.c $(CONTROL)controllerss.c $(CONTROL)controllersr.c \
										$(CONTROL)controllersrr.c  $(CONTROL)controllers.c \
				\
				$(SORT)root.c $(SORT)tools.c $(SORT)repeat.c $(SORT)choice.c \
											$(SORT)insert.c $(SORT)permanent.c \
				\
				$(PRINT)print.c  $(PRINT)bar.c $(PRINT)stack.c $(PRINT)tools.c \
				\
				$(READ)read.c

SRC_FILES1 = $(SRC)checker.c $(SRCS_PATH)
SRC_FILES2 = $(SRC)push_swap.c $(SRCS_PATH)

LINK = -lmlx -framework OpenGL -framework AppKit -L /usr/local/lib/

OBJS_NAME1 = $(SRC_FILES1:.c=.o)
OBJS_NAME2 = $(SRC_FILES2:.c=.o)

INCLUDES_PATH = includes

INCLUDES_NAME = push_swap.h libft.h
INCLUDES = $(addprefix $(INCLUDES_PATH)/,$(INCLUDE_NAME))

CFLAGS = -Wall -Wextra -Werror

LIBFT_PATH = libft
LIB = $(addprefix $(LIBFT_PATH)/,libft.a)

.SILENT: all, clean, fclean, re
.PHONY: all, clean, fclean, re

all: lib $(NAME)

$(NAME1): $(OBJS_NAME1)
	@gcc $(CFLAGS) -o $(NAME1) $(LINK) $(OBJS_NAME1) libft/libft.a

$(NAME2): $(OBJS_NAME2)
	@gcc $(FLAGS) -o $(NAME2) $(LINK) $(OBJS_NAME2) libft/libft.a

lib:
	@make -C libft/ all

clean:
	@/bin/rm -rf $(OBJS_NAME1) $(OBJS_NAME2)
	@make -C libft/ clean

fclean: clean
	@/bin/rm -f $(NAME1) $(NAME2)
	@make -C libft/ fclean

re: fclean all
