/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkoch <gkoch@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/05 19:01:31 by gkoch             #+#    #+#             */
/*   Updated: 2019/02/17 18:29:18 by gkoch            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "./libft.h"
# include "../mlx/mlx.h"
# include <fcntl.h>
# include <math.h>
# include <stdio.h>

# define MIN -2147483648
# define MAX 2147483647

typedef	struct			s_root
{
	int					move;
	struct s_stack		*a;
	struct s_stack		*b;
	struct s_history	*history;
	struct s_picture	*pic;
	char				**flags;
}						t_root;

typedef	struct			s_stack
{
	int					num;
	struct s_stack		*next;
}						t_stack;

typedef struct			s_history
{
	char				*value;
	struct s_history	*next;
}						t_history;

typedef struct			s_valid
{
	int					move;
	int					left;
	int					right;
	struct s_valid		*next;
}						t_valid;

typedef struct			s_move
{
	int					a;
	int					b;
	int					ab;
	struct s_move		*next;
}						t_move;

typedef	struct			s_picture
{
	int					width;
	int					height;
	void				*mlx_ptr;
	void				*win_ptr;
	struct s_map		*map;
}						t_picture;

typedef	struct			s_map
{
	struct s_line		*line;
	struct s_map		*next;
}						t_map;

typedef	struct			s_line
{
	int					color;
	struct s_line		*next;
}						t_line;

/*
** -------------------------- Create --------------------------------
*/
t_root					*create_root();
t_stack					*create_stack(int num);
t_history				*create_history(char *str);
t_valid					*create_valid(int move, int right, int left);
t_move					*create_move(int a, int b);
t_picture				*create_picture(int width, int height);
t_map					*create_map(t_line *line);
t_line					*create_line(int color);
/*
** -------------------------- Push --------------------------------
*/
int						push_history(t_history **begin_list, t_history *data);
int						push_stack(t_stack **begin_list, t_stack *data);
int						push_valid(t_valid **begin_list, t_valid *data);
int						push_move(t_move **begin_list, t_move *data);
/*
** -------------------------- Put --------------------------------
*/
void					puthistory(t_history *history);
void					putroot(t_root *root);
void					puterror();
/*
** -------------------------- Remove --------------------------------
*/
void					remove_firstack(t_stack **data);
void					remove_stack(t_stack **data);
void					remove_move(t_move **data);
void					remove_valid(t_valid **data);
/*
** -------------------------- Get Elem --------------------------------
*/
int						end_num(t_stack *stack);
int						before_last(t_stack *stack);
/*
** -------------------------- Clear --------------------------------
*/
void					clearroot(t_root **root);
void					clear_move(t_move **data);
/*
** -------------------------- Read --------------------------------
*/
void					ft_read(int ac, char **av, t_root **root);
/*
** -------------------------- Controllers --------------------------------
*/
int						sa(t_root **root);
int						sb(t_root **root);
int						ss(t_root **root);
int						pa(t_root **root);
int						pb(t_root **root);
int						ra(t_root **root);
int						rb(t_root **root);
int						rr(t_root **root);
int						rra(t_root **root);
int						rrb(t_root **root);
int						rrr(t_root **root);
void					universalr(t_stack **stack);
/*
** -------------------------- Controllers for checker----------------------
*/
void					controllers(t_root **root, char *str, int i);
/*
** -------------------------- Sort --------------------------------
*/
void					sort(t_root **root);
void					choice(t_root **root);
int						insert(t_stack *stack, t_stack *spare, int n);
int						checksorted(t_stack *stack);
/*
** -------------------------- Tools for sort --------------------------------
*/
int						countstack(t_stack *stack);
int						getmin(int a, int b);
int						getmax(int a, int b);
int						setplay(t_stack *stack, int count);
void					unzip3(t_root **root);
/*
** -------------------------- Repeat --------------------------------
*/
void					repeatrr(int a, t_root **root);
void					repeata(int a, t_root **root);
void					repeatb(int a, t_root **root);
void					repeatpb(int a, t_root **root);
/*
** -------------------------- Print --------------------------------
*/
void					printbar(t_root **root);
void					startdebug(t_root **root);
void					printstacka(t_root **root, t_stack *stack);
void					printstackb(t_root **root, t_stack *stack);
/*
** -------------------------- Tools for checker --------------------------------
*/
void					checkerres(t_root **root);
void					runworker(t_root **root);
int						getflag(char **flag, char *str);
void					worker(t_root **root);

#endif
